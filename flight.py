from flask import Flask, jsonify, make_response, url_for, request, render_template, redirect, session
from flaskext.mysql import MySQL
import time
import hashlib

app = Flask(__name__)
mysql = MySQL()

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'gravity_wins'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

# +------------+---------+------+-----+---------+----------------+
# | Field      | Type    | Null | Key | Default | Extra          |
# +------------+---------+------+-----+---------+----------------+
# | id         | int(11) | NO   | PRI | NULL    | auto_increment |
# | meet_id    | int(11) | YES  | MUL | NULL    |                |
# | is_deleted | int(11) | YES  |     | NULL    |                |
# +------------+---------+------+-----+---------+----------------+

def create_flight(meet_id):
  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("INSERT INTO flights (meet_id, is_deleted) VALUES ('%s', 0)" % (meet_id))
  conn.commit()
  cursor.close()

  return make_response(jsonify({'status': 'success'}))

def get_flight_by_id(id):
  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("SELECT * FROM flights WHERE id = '%s' AND (is_deleted = 0 OR is_deleted IS NULL);" % (id))
  flight = cursor.fetchone()
  cursor.close()

  if flight is None:
    return make_response(jsonify({'error': 'Not found'}), 404)

  return make_response(jsonify(format_flight(flight)))

def get_all_flights_for_meet(meet_id):
  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("SELECT * FROM flights WHERE meet_id = '%s'" % (meet_id))
  flights = cursor.fetchall()
  cursor.close()

  if len(flights) == 0:
    return make_response(jsonify({'flights': [], 'meet_id': meet_id}))

  return make_response(jsonify(format_flights(flights, meet_id)))

def format_flights(flights, meet_id):
  formatted_flights = []
  for flight in flights:
    print flight
    formatted_meet = {
      'id': flight[0],
    }
    formatted_flights.append(formatted_meet)
  return {'flights': formatted_flights, 'meet_id': meet_id}

def format_flight(flight):
  return {
    'id': flight[0],
    'meet_id': flight[1]
  }

def is_valid_flight_id(id, meet_id):
  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("SELECT * FROM flights WHERE id = '%s' AND (is_deleted = 0 OR is_deleted IS NULL) AND meet_id = '%s';" % (id, meet_id))
  meet = cursor.fetchone()
  cursor.close()

  if meet is None:
    return False
  return True
