from flask import Flask, jsonify, make_response, url_for, request, render_template, redirect, session
from flaskext.mysql import MySQL

app = Flask(__name__)
mysql = MySQL()

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'gravity_wins'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

# +------------+------------------------------------------+------+-----+---------+----------------+
# | Field      | Type                                     | Null | Key | Default | Extra          |
# +------------+------------------------------------------+------+-----+---------+----------------+
# | id         | int(11)                                  | NO   | PRI | NULL    | auto_increment |
# | meet_id    | int(11)                                  | YES  | MUL | NULL    |                |
# | lifter_id  | int(11)                                  | YES  | MUL | NULL    |                |
# | type       | enum('squat','bench','press','deadlift') | YES  |     | NULL    |                |
# | weight_kg  | int(11)                                  | YES  |     | NULL    |                |
# | weight_lb  | int(11)                                  | YES  |     | NULL    |                |
# | score_1    | int(11)                                  | YES  |     | NULL    |                |
# | score_2    | int(11)                                  | YES  |     | NULL    |                |
# | score_3    | int(11)                                  | YES  |     | NULL    |                |
# | is_deleted | int(11)                                  | YES  |     | NULL    |                |
# +------------+------------------------------------------+------+-----+---------+----------------+

def create_attempt(meet_id, lifter_id, type, weight_kg, weight_lb):
  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("INSERT INTO attempts (meet_id, lifter_id, type, weight_kg, weight_lb, is_deleted) VALUES ('%s', '%s','%s','%s','%s', 0)" % (meet_id, lifter_id, type, weight_kg, weight_lb))
  conn.commit()
  cursor.close()

  return make_response(jsonify({'status': 'success'}))

def get_attempt_by_id(id, lifter_id):
  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("SELECT * FROM attempts WHERE id = '%s' AND (is_deleted = 0 OR is_deleted IS NULL) AND lifter_id = '%s';" % (id, lifter_id))
  attempt = cursor.fetchone()
  cursor.close()

  if attempt is None:
    return make_response(jsonify({'error': 'Not found'}), 404)

  return make_response(jsonify(format_attempt(attempt)))

def get_all_attempts_for_lifter(lifter_id):
  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("SELECT * FROM attempts WHERE lifter_id = '%s'" % (lifter_id))
  attempts = cursor.fetchall()
  cursor.close()

  if len(attempts) == 0:
    return make_response(jsonify({'attempts': [], 'lifter_id': meet_id}))

  return make_response(jsonify(format_attempts(attempts, lifter_id)))

def format_attempts(attempts, lifter_id):
  formatted_attempts = []
  for lifter in lifters:
    formatted_attempt = format_attempt(attempt)
    formatted_attempts.append(formatted_attempt)
  return {'attempts': formatted_attempts, 'lifter_id': lifter_id}

def format_attempt(attempt):
  return {
    'id': attempt[0],
    'meet_id': attempt[1],
    'lifter_id': attempt[2],
    'type': attempt[3],
    'weight_kg': attempt[4],
    'weight_lb': attempt[5],
    'score_1': attempt[6],
    'score_2': attempt[7],
    'score_3': attempt[8],
  }

def is_valid_attempt_id(id, lifter_id):
  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("SELECT * FROM attempts WHERE id = '%s' AND (is_deleted = 0 OR is_deleted IS NULL) AND lifter_id = '%s';" % (id, lifter_id))
  attempt = cursor.fetchone()
  cursor.close()

  if attempt is None:
    return False
  return True
