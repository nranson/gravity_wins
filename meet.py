from flask import Flask, jsonify, make_response, url_for, request, render_template, redirect, session
from flaskext.mysql import MySQL
import time
import hashlib

app = Flask(__name__)
mysql = MySQL()

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'gravity_wins'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

# +------------+--------------+------+-----+---------+----------------+
# | Field      | Type         | Null | Key | Default | Extra          |
# +------------+--------------+------+-----+---------+----------------+
# | id         | int(11)      | NO   | PRI | NULL    | auto_increment |
# | name       | varchar(255) | YES  |     | NULL    |                |
# | user_id    | int(11)      | YES  | MUL | NULL    |                |
# | status     | varchar(255) | YES  |     | NULL    |                |
# | created_at | datetime     | YES  |     | NULL    |                |
# | is_deleted | int(11)      | YES  |     | NULL    |                |
# +------------+--------------+------+-----+---------+----------------+


def create_meet(user, name):
  user_id = user[0] #always the uid based on apikey
  status = "draft"
  created_at = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
  is_deleted = 0

  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("INSERT INTO meets (name, user_id, status, created_at, is_deleted) VALUES ('%s', '%s', '%s', '%s', '%s')" % (name, user_id, status, created_at, is_deleted))
  conn.commit()
  cursor.close()

  return make_response(jsonify({'status': 'success'}))

def get_meet_by_id(id):
  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("SELECT * FROM meets WHERE id = '%s' AND (is_deleted = 0 OR is_deleted IS NULL);" % (id))
  meet = cursor.fetchone()
  cursor.close()

  if meet is None:
    return make_response(jsonify({'error': 'Not found'}), 404)

  return make_response(jsonify(format_meet(meet)))

def get_all_meets_for_user(user):
  user_id = user[0] #always the uid based on apikey
  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("SELECT * FROM meets WHERE user_id = '%s';" % (user_id))
  meets = cursor.fetchall()
  cursor.close()

  if len(meets) == 0:
    return make_response(jsonify({'error': 'Not found'}), 404)

  return make_response(jsonify(format_meets(meets)))

def format_meets(meets):
  formatted_meets = []
  for meet in meets:
    formatted_meet = format_meet(meet)
    formatted_meets.append(formatted_meet)
  return {'meets': formatted_meets}

def format_meet(meet):
  return {
    'id': meet[0],
    'name': meet[1],
    'status': meet[3],
    'created_at': meet[4]
  }

def is_valid_meet_id(id, user):
  user_id = user[0]
  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("SELECT * FROM meets WHERE id = '%s' AND (is_deleted = 0 OR is_deleted IS NULL) AND user_id = '%s';" % (id, user_id))
  meet = cursor.fetchone()
  cursor.close()

  if meet is None:
    return False
  return True
