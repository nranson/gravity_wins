from flask import Flask, jsonify, make_response, url_for, request, render_template, redirect, session
from flaskext.mysql import MySQL
import time
import hashlib

app = Flask(__name__)
mysql = MySQL()

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'gravity_wins'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

# +-------------+--------------+------+-----+---------+----------------+
# | Field       | Type         | Null | Key | Default | Extra          |
# +-------------+--------------+------+-----+---------+----------------+
# | id          | int(11)      | NO   | PRI | NULL    | auto_increment |
# | meet_id     | int(11)      | YES  | MUL | NULL    |                |
# | flight_id   | int(11)      | YES  | MUL | NULL    |                |
# | first_name  | varchar(255) | YES  |     | NULL    |                |
# | last_name   | varchar(255) | YES  |     | NULL    |                |
# | weightclass | varchar(255) | YES  |     | NULL    |                |
# | is_deleted  | int(11)      | YES  |     | NULL    |                |
# +-------------+--------------+------+-----+---------+----------------+


def create_lifter(meet_id, flight_id, first_name, last_name, weightclass):
  if flight_id is None:
    flight_id = 0

  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("INSERT INTO lifters (meet_id, flight_id, first_name, last_name, weightclass, is_deleted) VALUES ('%s', '%s', '%s', '%s', '%s', 0)" % (meet_id, flight_id, first_name, last_name, weightclass))
  conn.commit()
  cursor.close()

  return make_response(jsonify({'status': 'success'}))

def get_lifter_by_id(id):
  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("SELECT * FROM lifters WHERE id = '%s' AND (is_deleted = 0 OR is_deleted IS NULL);" % (id))
  lifter = cursor.fetchone()
  cursor.close()

  if lifter is None:
    return make_response(jsonify({'error': 'Not found'}), 404)

  return make_response(jsonify(format_lifter(lifter)))

def get_all_lifters_for_meet(meet_id):
  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("SELECT * FROM lifters WHERE meet_id = '%s'" % (meet_id))
  lifters = cursor.fetchall()
  cursor.close()

  if len(lifters) == 0:
    return make_response(jsonify({'lifters': [], 'meet_id': meet_id}))

  return make_response(jsonify(format_lifters(lifters, meet_id)))

def get_all_lifters_for_flight(meet_id, flight_id):
  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("SELECT * FROM lifters WHERE flight_id = '%s' AND meet_id = '%s'" % (flight_id, meet_id))
  lifters = cursor.fetchall()
  cursor.close()

  if len(lifters) == 0:
    return make_response(jsonify({'lifters': [], 'flight_id': flight_id, 'meet_id': meet_id}))

  return make_response(jsonify(format_lifters_for_flight(lifters, meet_id, flight_id)))

def format_lifters(lifters, meet_id):
  formatted_lifters = []
  for lifter in lifters:
    formatted_lifter = format_lifter(lifter)
    formatted_lifters.append(formatted_lifter)
  return {'lifters': formatted_lifters, 'meet_id': meet_id}


def format_lifters_for_flight(lifters, meet_id, flight_id):
  formatted_lifters = []
  for lifter in lifters:
    formatted_lifter = format_lifter(lifter)
    formatted_lifters.append(formatted_lifter)
  return {'lifters': formatted_lifters, 'meet_id': meet_id, 'flight_id': flight_id}

def format_lifter(lifter):
  return {
    'id': lifter[0],
    'meet_id': lifter[1],
    'flight_id': lifter[2],
    'first_name': lifter[3],
    'last_name': lifter[4],
    'weightclass': lifter[5]
  }

def is_valid_lifter_id(meet_id, id):
  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("SELECT * FROM lifters WHERE id = '%s' AND (is_deleted = 0 OR is_deleted IS NULL) AND meet_id = '%s';" % (id, meet_id))
  lifter = cursor.fetchone()
  cursor.close()

  if lifter is None:
    return False
  return True
