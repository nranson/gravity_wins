import json
import httplib2
from flask import Flask, jsonify, make_response, url_for, request, render_template, redirect, session
from apiclient import discovery
from oauth2client import client
from flaskext.mysql import MySQL
from utilities import log_event, check_key, login_or_register, refresh_token, get_headers, get_user_from_key
from meet import create_meet, get_meet_by_id, get_all_meets_for_user, is_valid_meet_id
from flight import create_flight, get_flight_by_id, get_all_flights_for_meet, is_valid_flight_id
from lifter import create_lifter, get_lifter_by_id, get_all_lifters_for_flight, get_all_lifters_for_meet, is_valid_lifter_id
from attempt import create_attempt, get_all_attempts_for_lifter, get_attempt_by_id

############
## CONFIG ##
############
app = Flask(__name__)
mysql = MySQL()
app.secret_key = 'cfb_api_localhost'

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'gravity_wins'
app.config['MYSQL_DATABASE_HOST'] = '127.0.0.1'
mysql.init_app(app)

##################
## INDEX ROUTES ##
##################

@app.errorhandler(404)
def not_found(error):
  response = make_response(jsonify({'error': 'Not found'}), 404)
  return response

@app.route('/', methods=['GET'])
def index():
  return render_template('index.html')

@app.route('/login')
def login():
  """The login method using Google's OAuth2 for authentication."""
  if 'credentials' not in session:
    return redirect(url_for('oauth2callback'))
  credentials = client.OAuth2Credentials.from_json(session['credentials'])
  if credentials.access_token_expired:
    return redirect(url_for('oauth2callback'))
  else:
    http_auth = credentials.authorize(httplib2.Http())
    profile = discovery.build('plus', 'v1', http_auth)
    me = profile.people().get(userId='me').execute()

    user = login_or_register(me)
    return user

@app.route('/oauth2callback')
def oauth2callback():
  flow = client.flow_from_clientsecrets(
      # '/var/www/Flask/FlaskApp/client_secrets.json',
      'client_secrets.json',
      scope='https://www.googleapis.com/auth/userinfo.email',
      redirect_uri=url_for('oauth2callback', _external=True))
  if 'code' not in request.args:
    auth_uri = flow.step1_get_authorize_url()
    return redirect(auth_uri)
  else:
    auth_code = request.args.get('code')
    credentials = flow.step2_exchange(auth_code)
    session['credentials'] = credentials.to_json()
    return redirect(url_for('login'))

#########
# MEETS #
#########
@app.route('/create/meet')
def create_meet():
    key = request.args.get('key')
    if key is None:
        response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
        return response
    name = request.args.get('name')
    if name is None:
        response = make_response(jsonify({'error': 'Missing required field', 'message': 'A name is required for a meet.'}), 401)
        return response
    return create_meet(get_user_from_key(key), name)

@app.route('/get/meet')
def get_meets():
    key = request.args.get('key')
    if key is None:
        response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
        return response
    return get_all_meets_for_user(get_user_from_key(key))

@app.route('/get/meet/<int:meet_id>')
def get_meet(meet_id):
    key = request.args.get('key')
    if key is None:
        response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
        return response
    if meet_id is None:
        response = make_response(jsonify({'error': 'Missing required field', 'message': 'An id is required for a meet.'}), 401)
        return response
    return get_meet_by_id(meet_id)

###########
# FLIGHTS #
###########
@app.route('/create/meet/<int:meet_id>/flight')
def create_flight_for_meet(meet_id):
    key = request.args.get('key')
    if key is None:
        response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
        return response
    if is_valid_meet_id(meet_id, get_user_from_key(key)):
        return create_flight(meet_id)
    return make_response(jsonify({'error': 'Missing required field', 'message': 'No meet was found with that id.'}), 401)

@app.route('/get/meet/<int:meet_id>/flight')
def get_flight_for_meet(meet_id):
    key = request.args.get('key')
    if key is None:
        response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
        return response
    if is_valid_meet_id(meet_id, get_user_from_key(key)):
        return get_all_flights_for_meet(meet_id)
    return make_response(jsonify({'error': 'Missing required field', 'message': 'No meet was found with that id.'}), 401)

@app.route('/get/meet/<int:meet_id>/flight/<int:flight_id>')
def get_flight_for_meet_by_id(meet_id, flight_id):
    key = request.args.get('key')
    if key is None:
        response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
        return response
    if not is_valid_flight_id(flight_id, meet_id):
        return make_response(jsonify({'error': 'Missing required field', 'message': 'No flight was found with that id.'}), 401)
    if is_valid_meet_id(meet_id, get_user_from_key(key)):
        return get_flight_by_id(flight_id)
    return make_response(jsonify({'error': 'Missing required field', 'message': 'No meet was found with that id.'}), 401)

###########
# LIFTERS #
###########
@app.route('/create/meet/<int:meet_id>/lifter')
def create_lifter_for_meet(meet_id):
    first_name = request.args.get('first_name')
    last_name = request.args.get('last_name')
    weightclass = request.args.get('weightclass')
    flight_id = request.args.get('flight_id')

    key = request.args.get('key')
    if key is None:
        response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
        return response
    if is_valid_meet_id(meet_id, get_user_from_key(key)):
        return create_lifter(meet_id, flight_id, first_name, last_name, weightclass)
    return make_response(jsonify({'error': 'Missing required field', 'message': 'No meet was found with that id.'}), 401)

@app.route('/create/meet/<int:meet_id>/flight/<int:flight_id>/lifter')
def create_lifter_for_flight(meet_id, flight_id):
    first_name = request.args.get('first_name')
    last_name = request.args.get('last_name')
    weightclass = request.args.get('weightclass')

    key = request.args.get('key')
    if key is None:
        response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
        return response
    if not is_valid_flight_id(flight_id, meet_id):
        return make_response(jsonify({'error': 'Missing required field', 'message': 'No flight was found with that id.'}), 401)
    if is_valid_meet_id(meet_id, get_user_from_key(key)):
        return create_lifter(meet_id, flight_id, first_name, last_name, weightclass)
    return make_response(jsonify({'error': 'Missing required field', 'message': 'No meet was found with that id.'}), 401)

@app.route('/get/meet/<int:meet_id>/lifter')
def get_lifter_for_meet(meet_id):
    key = request.args.get('key')
    if key is None:
        response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
        return response
    if is_valid_meet_id(meet_id, get_user_from_key(key)):
        return get_all_lifters_for_meet(meet_id)
    return make_response(jsonify({'error': 'Missing required field', 'message': 'No meet was found with that id.'}), 401)

@app.route('/get/meet/<int:meet_id>/lifter/<int:lifter_id>')
def get_lifter_for_meet_by_id(meet_id, lifter_id):
    key = request.args.get('key')
    if key is None:
        response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
        return response
    if not is_valid_lifter_id(meet_id, lifter_id):
        return make_response(jsonify({'error': 'Missing required field', 'message': 'No lifter was found with that id.'}), 401)
    if is_valid_meet_id(meet_id, get_user_from_key(key)):
        return get_lifter_by_id(lifter_id)
    return make_response(jsonify({'error': 'Missing required field', 'message': 'No meet was found with that id.'}), 401)

@app.route('/get/meet/<int:meet_id>/flight/<int:flight_id>/lifter/<int:lifter_id>')
def get_lifter_for_flight_by_id(meet_id, flight_id, lifter_id):
    key = request.args.get('key')
    if key is None:
        response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
        return response
    if not is_valid_lifter_id(meet_id, lifter_id):
        return make_response(jsonify({'error': 'Missing required field', 'message': 'No lifter was found with that id.'}), 401)
    if not is_valid_flight_id(flight_id, meet_id):
        return make_response(jsonify({'error': 'Missing required field', 'message': 'No flight was found with that id.'}), 401)
    if is_valid_meet_id(meet_id, get_user_from_key(key)):
        return get_lifter_by_id(lifter_id)
    return make_response(jsonify({'error': 'Missing required field', 'message': 'No meet was found with that id.'}), 401)


@app.route('/get/meet/<int:meet_id>/flight/<int:flight_id>/lifter')
def get_lifter_for_flight(meet_id, flight_id):
    key = request.args.get('key')
    if key is None:
        response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
        return response
    if not is_valid_flight_id(flight_id, meet_id):
        return make_response(jsonify({'error': 'Missing required field', 'message': 'No flight was found with that id.'}), 401)
    if is_valid_meet_id(meet_id, get_user_from_key(key)):
        return get_all_lifters_for_flight(meet_id, flight_id)
    return make_response(jsonify({'error': 'Missing required field', 'message': 'No meet was found with that id.'}), 401)

############
# ATTEMPTS #
############
@app.route('/create/meet/<int:meet_id>/lifter/<int:lifter_id>/attempt/bench')
def create_bench_attempt_for_lifter(meet_id, lifter_id):
    weight_kg = request.args.get('weight_kg') if request.args.get('weight_kg') else 0
    weight_lb = request.args.get('weight_lb') if request.args.get('weight_lb') else 0

    key = request.args.get('key')
    if key is None:
        response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
        return response
    if not is_valid_lifter_id(meet_id, lifter_id):
        return make_response(jsonify({'error': 'Missing required field', 'message': 'No lifter was found with that id.'}), 401)
    if is_valid_meet_id(meet_id, get_user_from_key(key)):
        return create_attempt(meet_id, lifter_id, 'bench', weight_kg, weight_lb)
    return make_response(jsonify({'error': 'Missing required field', 'message': 'No meet was found with that id.'}), 401)

@app.route('/create/meet/<int:meet_id>/lifter/<int:lifter_id>/attempt/deadlift')
def create_deadlift_attempt_for_lifter(meet_id, lifter_id):
    weight_kg = request.args.get('weight_kg') if request.args.get('weight_kg') else 0
    weight_lb = request.args.get('weight_lb') if request.args.get('weight_lb') else 0

    key = request.args.get('key')
    if key is None:
        response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
        return response
    if not is_valid_lifter_id(meet_id, lifter_id):
        return make_response(jsonify({'error': 'Missing required field', 'message': 'No lifter was found with that id.'}), 401)
    if is_valid_meet_id(meet_id, get_user_from_key(key)):
        return create_attempt(meet_id, lifter_id, 'deadlift', weight_kg, weight_lb)
    return make_response(jsonify({'error': 'Missing required field', 'message': 'No meet was found with that id.'}), 401)

@app.route('/create/meet/<int:meet_id>/lifter/<int:lifter_id>/attempt/press')
def create_press_attempt_for_lifter(meet_id, lifter_id):
    weight_kg = request.args.get('weight_kg') if request.args.get('weight_kg') else 0
    weight_lb = request.args.get('weight_lb') if request.args.get('weight_lb') else 0

    key = request.args.get('key')
    if key is None:
        response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
        return response
    if not is_valid_lifter_id(meet_id, lifter_id):
        return make_response(jsonify({'error': 'Missing required field', 'message': 'No lifter was found with that id.'}), 401)
    if is_valid_meet_id(meet_id, get_user_from_key(key)):
        return create_attempt(meet_id, lifter_id, 'press', weight_kg, weight_lb)
    return make_response(jsonify({'error': 'Missing required field', 'message': 'No meet was found with that id.'}), 401)

@app.route('/create/meet/<int:meet_id>/lifter/<int:lifter_id>/attempt/squat')
def create_squat_attempt_for_lifter(meet_id, lifter_id):
    weight_kg = request.args.get('weight_kg') if request.args.get('weight_kg') else 0
    weight_lb = request.args.get('weight_lb') if request.args.get('weight_lb') else 0

    key = request.args.get('key')
    if key is None:
        response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
        return response
    if not is_valid_lifter_id(meet_id, lifter_id):
        return make_response(jsonify({'error': 'Missing required field', 'message': 'No lifter was found with that id.'}), 401)
    if is_valid_meet_id(meet_id, get_user_from_key(key)):
        return create_attempt(meet_id, lifter_id, 'squat', weight_kg, weight_lb)
    return make_response(jsonify({'error': 'Missing required field', 'message': 'No meet was found with that id.'}), 401)

@app.route('/test')
def test():
    key = request.args.get('key')
    user = get_user_from_key(key)
    return make_response(jsonify({'user': user[0], 'email': user[1]}))

if __name__ == '__main__':
  import uuid
  app.secret_key = str(uuid.uuid4())
  app.debug = True
  app.run()