from flask import Flask, jsonify, make_response, url_for, request, render_template, redirect, session
from flaskext.mysql import MySQL
import time
import hashlib

app = Flask(__name__)
mysql = MySQL()

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'gravity_wins'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

def log_event(status_code, key):
  conn   = mysql.connect()
  cursor = conn.cursor()
  
  current_time = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
  user_ip      = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
    
  cursor.execute("INSERT INTO api_logs (time, ip, url, http_method, user_agent, api_key, response) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', {6})".format(current_time, user_ip, request.url, request.method, request.headers.get('User-Agent'), key, status_code))
  conn.commit()
  cursor.close()

def check_key(key):
  conn   = mysql.connect()
  cursor = conn.cursor()
  user = cursor.execute("SELECT login_token FROM users WHERE login_token = '%s' ORDER BY id ASC LIMIT 1;" % key)
  keys = cursor.fetchone()
  if len(keys) is not 0:
    cursor.execute("UPDATE users SET request_count = (request_count + 1) WHERE apikey = '%s';" % key)
    conn.commit()
    conn.close()
    return
  else:
    conn.close()
    response = make_response(jsonify({'error': 'Not authorized', 'message': 'Check to make sure your api key is valid.'}), 401)
    return response

def get_headers(request):
  try:
    auth = request.authorization
    check_key(auth.password)
    return auth.password
  except:
    try:
      key = request.headers.get('Authorization')
      key = key.split(' ')
      check_key(key[1])
      return key[1]
    except:
      return False


def login_or_register(me):
    conn   = mysql.connect()
    cursor = conn.cursor()
    user = cursor.execute("SELECT * FROM users WHERE email_address = '%s';" % me['emails'][0]['value'])
    users = cursor.fetchall()
    if len(users) == 0:
      md5 = hashlib.md5()
      current_time = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
      key = str(current_time) + me['emails'][0]['value']
      md5.update(key)
      api_key = md5.hexdigest()
      api_key = api_key
      cursor.execute("INSERT INTO users (email_address, login_token) VALUES ('%s', '%s')" % (me['emails'][0]['value'], api_key))
      conn.commit()
      cursor.close()
      return render_template('profile.html', api_key=api_key, email_address=me['emails'][0]['value'], name=me['displayName'], image=me['image']['url'])
    else:
      cursor.close()
      for user in users:
        api_key = user[4]
        name    = user[0]
        image   = user[2].split('?')
        email   = user[1]
      return render_template('profile.html', api_key=api_key, email_address=email, name=name, image=image[0])

def refresh_token(email):
  current_time = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
  key = str(current_time) + email
  md5 = hashlib.md5()
  md5.update(key)
  api_key = md5.hexdigest()

  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("UPDATE users SET login_token = '%s' WHERE email_address = '%s';" % (api_key, email))
  conn.commit()

  user = cursor.execute("SELECT * FROM users WHERE email_address = '%s';" % email)
  users = cursor.fetchall()

  cursor.close()

  for user in users:
    api_key = user[4]
    name    = user[0]
    image   = user[2].split('?')
    email   = user[1]
  return render_template('profile.html', api_key=api_key, email_address=email, name=name, image=image[0])

def get_user_from_key(key):
  conn   = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("SELECT id, email_address FROM users WHERE login_token = '%s';" % (key))
  user = cursor.fetchone()
  cursor.close()
  return user